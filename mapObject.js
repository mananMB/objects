const mapObject = (obj, cb) => {
  let newObject = {};
  for (let key in obj) {
    const result = cb(obj[key], key, obj);
    newObject[result[0]] = result[1];
  }
  return newObject;
};

module.exports = mapObject;
