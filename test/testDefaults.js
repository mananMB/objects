const expect = require("chai").expect;
const defaults = require("../defaults");
const testObject = require("../objects");

describe("Test defaults module", () => {
  it("should return object after filling undefined properties from default object list", function () {
    const result = defaults(testObject, {
      location: "Gotham",
      car: "Bat Mobile",
    });
    expect(result).to.deep.equal({
      name: "Bruce Wayne",
      age: 36,
      location: "Gotham",
      car: "Bat Mobile",
    });
  });
});
