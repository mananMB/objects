const expect = require("chai").expect;
const keys = require("../keys");
const testObject = require("../objects");

describe("Test Keys module", () => {
  it("should return keys in the object", function () {
    const result = keys(testObject);
    expect(result).to.deep.equal(["name", "age", "location"]);
  });
});
