const expect = require("chai").expect;
const mapObject = require("../mapObject");
const testObject = require("../objects");

describe("Test mapObjects module", () => {
  it("should return a new object with operation performed on entries", function () {
    const result = mapObject(testObject, function (val, key) {
      return [key, val + 5];
    });
    expect(result).to.deep.equal({
      name: "Bruce Wayne5",
      age: 41,
      location: "Gotham5",
    });
  });
});
