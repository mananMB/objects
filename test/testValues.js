const expect = require("chai").expect;
const values = require("../values");
const testObject = require("../objects");

describe("Test Values module", () => {
  it("should return values in the object", function () {
    const result = values(testObject);
    expect(result).to.deep.equal(["Bruce Wayne", 36, "Gotham"]);
  });
  it("should ignore values which are a function", function () {
    const result = values({ aFunction: () => {}, name: "Bruce Wayne" });
    expect(result).to.deep.equal(["Bruce Wayne"]);
  });
});
