const expect = require("chai").expect;
const invert = require("../invert");
const testObject = require("../objects");

describe("Test invert module", () => {
  it("should return a new object with keys and values inverted", function () {
    const result = invert(testObject);
    expect(result).to.deep.equal({
      "Bruce Wayne": "name",
      36: "age",
      Gotham: "location",
    });
  });
});
