const expect = require("chai").expect;
const pairs = require("../pairs");
const testObject = require("../objects");

describe("Test pairs module", () => {
  it("should return an array containing pairs of the object", function () {
    const result = pairs(testObject);
    expect(result).to.deep.equal([
      ["name", "Bruce Wayne"],
      ["age", 36],
      ["location", "Gotham"],
    ]);
  });
});
