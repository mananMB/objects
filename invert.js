const invert = (obj) => {
  const inverted = {};
  for (const key in obj) {
    inverted[obj[key]] = key;
  }
  return inverted;
};

module.exports = invert;
