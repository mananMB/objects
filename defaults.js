const defaults = (obj, defaultProps) => {
  const newObject = Object.assign({}, obj);
  for (const defaultPropsKey in defaultProps) {
    if (newObject[defaultPropsKey] === undefined) {
      newObject[defaultPropsKey] = defaultProps[defaultPropsKey];
    }
  }
  return newObject;
};

module.exports = defaults;
