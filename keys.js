const keys = (obj) => {
  let keys = [];
  for (const objKey in obj) {
    keys.push(objKey);
  }
  return keys;
};

module.exports = keys;
