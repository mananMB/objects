const values = (obj) => {
  let values = [];
  for (const objKey in obj) {
    if (typeof obj[objKey] === "function") continue;
    values.push(obj[objKey]);
  }
  return values;
};

module.exports = values;
