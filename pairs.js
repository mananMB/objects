const pairs = (obj) => {
  const pairsArray = [];
  for (const objKey in obj) {
    pairsArray.push([objKey, obj[objKey]]);
  }
  return pairsArray;
};

module.exports = pairs;
